const { range } = require('./utils')

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/find
// arr.find(callback[, thisArg])

// Ejemplo 1, obtener un elemento de un arreglo:
let numberArray = range(10)

const toFind = 4

// for
let number = 0
for (let counter = 0; counter < numberArray.length; counter++) {
  let condition = (numberArray[counter] % toFind === 0) && (numberArray[counter] / toFind) > 1
  if (condition) number = numberArray[counter]
}
console.log('Encontrar un número del arreglo con for:\n', number)

// forEach
let number1 = 0
numberArray.forEach(number => {
  let condition = (number % toFind === 0) && (number / toFind) > 1
  if (condition) number1 = number
})
console.log('Encontrar un número del arreglo con forEach:\n', number1)

// find
let number2 = numberArray.find(number => (number % toFind === 0) && (number / toFind) > 1)
console.log('Encontrar un número del arreglo con find:\n', number2)

// Ejemplo 2, obtener un objeto desde un arreglo:
let persons = [
  {id: 1, name: 'Ethien', tag: 'java'},
  {id: 2, name: 'Iddar', tag: 'javascript'},
  {id: 3, name: 'Erick', tag: 'javascript'},
  {id: 4, name: 'Edgar', tag: 'javascript'},
  {id: 5, name: 'Carlos', tag: 'java'}
]
let person = persons.find(person => person.id === 2)
console.log('Obtener un objeto desde un arreglo:\n', person)
