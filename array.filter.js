const { range } = require('./utils')

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter
// arr.filter(callback[, thisArg])

// Primer ejemplo numeros pares:
let numberArray = range(10)

// for
let evenNumbers = []
for (let counter = 0; counter < numberArray.length; counter++) {
  if (numberArray[counter] % 2 === 0) {
    evenNumbers.push(numberArray[counter])
  }
}
console.log('Números pares con for:\n', evenNumbers)

// Números pares con forEach
let evenNumbers1 = []
numberArray.forEach(number => {
  if (number % 2 === 0) evenNumbers1.push(number)
})
console.log('Números pares con forEach:\n', evenNumbers1)

// filter
let evenNumbers2 = numberArray.filter(number => number % 2 === 0)
console.log('Números pares con filter:\n', evenNumbers2)

// Segundo ejemplo filtrar por etiqueta javascript:
let persons = [
  {id: 1, name: 'Ethien', tag: 'java'},
  {id: 2, name: 'Iddar', tag: 'javascript'},
  {id: 3, name: 'Erick', tag: 'javascript'},
  {id: 4, name: 'Edgar', tag: 'javascript'},
  {id: 5, name: 'Carlos', tag: 'java'}
]

let filteredPersons = persons.filter(person => person.tag.includes('javascript'))
console.log('Personas filtradas por tag:\n', filteredPersons)
