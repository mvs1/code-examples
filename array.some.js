const { range } = require('./utils')

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/some
// arr.some(callback[, thisArg])

// Primer ejemplo arreglo contiene 5:
let numberArray = range(10)

// for
let hasFive = false
for (let counter = 0; counter < numberArray.length; counter++) {
  if (numberArray[counter] === 5) {
    hasFive = true
    break
  }
}
console.log('El arreglo contiene un cinco con for:\n', hasFive)

// forEach
let hasFive1 = false
numberArray.forEach(number => {
  if (number === 5) {
    hasFive1 = true
  }
})
console.log('El arreglo contiene un cinco con forEach:\n', hasFive1)

// Some
let hasFive2 = numberArray.some(number => number === 5)
console.log('El arreglo contiene un cinco con some:\n', hasFive2)

let hasEleven = numberArray.some(number => number === 11)
console.log('El arreglo contiene un once con some:\n', hasEleven)

// Ejemplo 2, arreglo contiene un objeto con etiqueta java

let persons = [
  {id: 1, name: 'Ethien', tag: 'java'},
  {id: 2, name: 'Iddar', tag: 'javascript'},
  {id: 3, name: 'Erick', tag: 'javascript'},
  {id: 4, name: 'Edgar', tag: 'javascript'},
  {id: 5, name: 'Carlos', tag: 'java'}
]

let hasJava = persons.some(person => person.tag.includes('java'))
console.log('Un elemento del arreglo tiene java:\n', hasJava)
