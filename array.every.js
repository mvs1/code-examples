const { range } = require('./utils')

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/every
// arr.every(callback[, thisArg])

// Primer ejemplo un elemento del arreglo esta vacio:
let numberArray = range(10)

// for
let containEmpty = false
for (let counter = 0; counter < numberArray.length; counter++) {
  if (numberArray[counter] != null) {
    containEmpty = true
    break
  }
}
console.log('Contiene un elemento vacio con for:\n', containEmpty)

// forEach
let containEmpty1 = false
numberArray.forEach(number => {
  if (number != null) {
    containEmpty1 = true
  }
})
console.log('Contiene un elemento vacio con forEach:\n', containEmpty1)

// Every
let containEmpty2 = numberArray.every(number => number != null)
console.log('Contiene un elemento vacio con every:\n', containEmpty2)

numberArray[1] = null
let containEmpty3 = numberArray.every(number => number != null)
console.log('Contiene un elemento vacio con every:\n', containEmpty3)

// Segundo ejemplo, mientras ningún elemento tenga una etiqueta php:

let persons = [
  {id: 1, name: 'Ethien', tag: 'java'},
  {id: 2, name: 'Iddar', tag: 'javascript'},
  {id: 3, name: 'Erick', tag: 'javascript'},
  {id: 4, name: 'Edgar', tag: 'javascript'},
  {id: 5, name: 'Carlos', tag: 'php'}
]

let knowJavascript = persons.every(person => person.tag.includes('javascript'))
console.log('Todos los elementos del arreglo tienen javascript:\n', knowJavascript)
