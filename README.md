# Códigos de ejemplo para tipos de datos en Javascript

## Array

### Metodos:

1. __forEach__
2. __map__
3. __filter__
4. __some__
5. __reduce__
6. __find__

#### Ventajas: 

* Fácil de escribir y fácil de interpretar
* Fácil de mantener, extender y probar
* Puedes escribir funciones puras y usarlas dentro de los metodos

#### Otros metodos:

* pop
* push
* shift
* unshift
* splice
* concat
* slice
* min
* max
* sort
* toString


## [Array.prototype.map](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map)

* __callback__: Función que producirá un elemento del nuevo array, recibe tres argumentos:
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __thisArg__: _Opcional_. Valor a usar como this al ejecutar callback.

El callback debe retornar un nuevo elemento por cada posición en la que se trabaja.
__Devuelve__: 

## [Array.prototype.filter](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter)

* __callback__: Función que producirá un elemento del nuevo array, recibe tres argumentos:
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __thisArg__: _Opcional_. Valor a usar como this al ejecutar callback.

El callback debe retornar un boleano para la posición de trabajo actual.

## [Array.prototype.some](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/some)

* __callback__: Función que producirá un elemento del nuevo array, recibe tres argumentos:
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __thisArg__: _Opcional_. Valor a usar como this al ejecutar callback.

El callback debe retornar un boleano para la posición de trabajo actual.

## [Array.prototype.every](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/every)

* __callback__: Función que producirá un elemento del nuevo array, recibe tres argumentos:
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __thisArg__: _Opcional_. Valor a usar como this al ejecutar callback.

El callback debe retornar un boleano para la posición de trabajo actual.

## [Array.prototype.reduce](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/reduce)

* __callback__: Función a ejecutar para cada valor del array, recibe cuatro parametros:
  * *__lastValue__*: El valor devuelto (retornado) en la llamada anterior de la función, o el valorInicial, si se proveyó.
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __initialValue__: Valor inicial.

El callback debe retornar un valor.

## [Array.prototype.find](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/find)

* __callback__: Función que retornará el valor encontrado:
  * *__currentValue__*: El elemento actual del array que se está procesando.
  * *__index__*: El índice del elemento actual dentro del array.
  * *__array__*: El array sobre el que se llama map.
* __thisArg__: _Opcional_. Valor a usar como this al ejecutar callback.

El callback debe retornar un booleano, y find devolvera un valor en el array si el elemento pasa la prueba; de lo contrario, undefined.
