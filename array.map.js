// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map
// var nuevo_array = arr.map(function callback(currentValue, index, array) {
//   Elemento devuelto de nuevo_array
// }[, thisArg])

// Ejemplo 1, el cuadrado de los Números:
let numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// for
let squareNumbers = []
for (let counter = 0; counter < numberArray.length; counter++) {
  squareNumbers.push(numberArray[counter] * numberArray[counter])
}
console.log('El cuadrado de los números con for:\n', squareNumbers)

// forEach
let squareNumbers1 = []
numberArray.forEach(number => {
  squareNumbers1.push(number * number)
})
console.log('El cuadrado de los números con forEach:\n', squareNumbers1)

// Map
let squareNumbers2 = numberArray.map(number => number * number)
console.log('El cuadrado de los números con map:\n', squareNumbers2)

// Ejemplo 2, arreglo de objetos a arreglo de strings:
let persons = [
  {id: 1, name: 'Ethien'},
  {id: 2, name: 'Iddar'},
  {id: 3, name: 'Erick'},
  {id: 4, name: 'Edgar'},
  {id: 5, name: 'Carlos'}
]

let nameArray = persons.map(personObj => personObj.name)
console.log('Arreglo de objetos a arreglo de strings: \n', nameArray)
