// eslint-disable-next-line
function oldRangeFor (number) {
  let numberArrayDynamic = []
  for (let counter = 0; counter < number; counter++) {
    numberArrayDynamic.push(counter)
  }
  return numberArrayDynamic
}

// eslint-disable-next-line
function oldRangeForEach (number) {
  let numberArrayDynamic = []
  Array(number).fill(0).forEach((value, index) => {
    numberArrayDynamic.push(index + 1)
  })
  return numberArrayDynamic
}

module.exports = {
  range: (number) => Array(number).fill(0).map((value, index) => index + 1)
}
