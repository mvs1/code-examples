const { range } = require('./utils')

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/reduce
// var resultado = arr.reduce(funcion[, valorInicial])

// Ejemplo 1, suma de un arreglo:
let numberArray = range(10)

// for
let sum = 0
for (let counter = 0; counter < numberArray.length; counter++) {
  sum += numberArray[counter]
}
console.log('Suma de un arreglo con for:\n', sum)

// forEach
let sum1 = 0
numberArray.forEach(number => {
  sum1 += number
})
console.log('Suma de un arreglo con forEach:\n', sum1)

// Reduce
let sum2 = numberArray.reduce((reducer, value) => reducer + value, 0)
console.log('Suma de un arreglo con reduce:\n', sum2)

// Ejemplo 2, crear un objeto desde un arreglo:
let persons = [
  {id: 1, name: 'Ethien', tag: 'java'},
  {id: 2, name: 'Iddar', tag: 'javascript'},
  {id: 3, name: 'Erick', tag: 'javascript'},
  {id: 4, name: 'Edgar', tag: 'javascript'},
  {id: 5, name: 'Carlos', tag: 'java'}
]
let uniqueTags = persons.reduce((reducer, person) => {
  if (person.tag === 'javascript') reducer[person.name] = {id: person.id, good: true}
  return reducer
}, {})
console.log('Un nuevo objeto desde un arreglo:\n', uniqueTags)

// Ejemplo 3, un solo arreglo de columnas
let categories = [{
  type: 'category1',
  cols: ['col A', 'col B']
}, {
  type: 'category2',
  cols: ['col C', 'col D', 'col E']
}, {
  type: 'category3',
  cols: ['col F']
}]
let flatenList = categories.reduce((acc, category) => {
  acc = acc.concat(category.cols)
  return acc
}, [])
console.log('Un arreglo plano con reduce: \n', flatenList)
